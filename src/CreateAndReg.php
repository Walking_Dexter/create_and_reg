<?php

namespace Drupal\create_and_reg;

use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Utility\Token;
use Drupal\Core\Url;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\Crypt;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;

/**
 * Provides the create_and_reg service.
 */
class CreateAndReg {

  /**
   * Don't redirect the user.
   */
  const REDIRECT_NONE = 0;

  /**
   * Redirect the user to the login page.
   */
  const REDIRECT_LOGIN_PAGE = 1;

  /**
   * Redirect the user to the registration page.
   */
  const REDIRECT_REGISTRATION_PAGE = 2;

  /**
   * Redirect the user to the other url.
   */
  const REDIRECT_OTHER_URL = 3;

  /**
   * The name of the cookie to store the session ID.
   */
  const COOKIE_NAME = 'create_and_reg';

  /**
   * A config object for the 'create_and_reg.settings' configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The storage of node entities.
   *
   * @var \Drupal\node\NodeStorageInterface
   */
  protected $nodeStorage;

  /**
   * The current active database's master connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * CreateAndReg constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Database\Connection $database
   *   The current active database's master connection.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, Connection $database, RequestStack $request_stack, TimeInterface $time, Token $token) {
    $this->config = $config_factory->get('create_and_reg.settings');
    $this->nodeStorage = $entity_type_manager->getStorage('node');
    $this->database = $database;
    $this->requestStack = $request_stack;
    $this->time = $time;
    $this->token = $token;
  }

  /**
   * Gets the current session ID.
   *
   * @return string
   *   The session ID for use in this module.
   */
  public function getCurrentSession() {
    $request = $this->requestStack->getCurrentRequest();

    if ($request->cookies->has(static::COOKIE_NAME)) {
      $session_id = $request->cookies->get(static::COOKIE_NAME);
    }
    else {
      $session_id = Crypt::randomBytesBase64();
      setcookie(static::COOKIE_NAME, $session_id, 0, base_path());
    }
    return $session_id;
  }

  /**
   * Saves the passed session to the database.
   *
   * @param string $session_id
   *   The session ID.
   * @param int $nid
   *   The node ID associated with the session.
   */
  public function saveSession($session_id, $nid) {
    $this->database->merge('create_and_reg')
      ->key(['nid' => $nid])
      ->fields([
        'session_id' => $session_id,
        'timestamp' => $this->time->getRequestTime(),
      ])
      ->execute();
  }

  /**
   * Removes expired sessions.
   */
  public function deleteExpiredSessions() {
    $expire = $this->time->getRequestTime() - $this->config->get('session_lifetime');
    $this->database->delete('create_and_reg')
      ->condition('timestamp', $expire, '<')
      ->execute();
  }

  /**
   * Removes the session by the node ID.
   *
   * @param int $nid
   *   The node ID for which to remove the session.
   */
  public function deleteSessionByNid($nid) {
    $this->database->delete('create_and_reg')
      ->condition('nid', $nid)
      ->execute();
  }

  /**
   * Gets the node ids connected to the passed session.
   *
   * @param string|null $session_id
   *   Anonymous user session_id in create_and_reg table.
   *
   * @return \Drupal\node\NodeInterface[]
   *   Array of nodes.
   */
  public function getNodes($session_id = NULL) {
    if (!isset($session_id)) {
      $session_id = $this->getCurrentSession();
    }

    $query = $this->database->select('create_and_reg', 'c');
    $query->addField('c', 'nid');
    $query->join('node_field_data', 'n', 'c.nid = n.nid');
    $query->condition('c.session_id', $session_id);
    $query->condition('n.uid', 0);
    $ids = $query->execute()->fetchCol();

    $nodes = [];
    if (!empty($ids)) {
      $nodes = $this->nodeStorage->loadMultiple($ids);
    }
    return $nodes;
  }

  /**
   * Assigns the nodes to the user.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user to whom the nodes are assigned.
   * @param string|null $session_id
   *   Anonymous user session_id in create_and_reg table.
   */
  public function assignNodes(UserInterface $account, $session_id = NULL) {
    foreach ($this->getNodes($session_id) as $node) {
      $node->setOwner($account);
      $node->save();

      // Remove the session.
      $this->deleteSessionByNid($node->id());
    }
  }

  /**
   * Gets the enabled node types.
   *
   * @return array
   *   An array of node types ids.
   */
  public function getEnabledTypes() {
    return $this->config->get('enabled_types');
  }

  /**
   * Returns whether or not passed node type is enabled.
   *
   * @param string $type
   *   The node type.
   *
   * @return bool
   *   TRUE if node type is enabled, FALSE otherwise.
   */
  public function isTypeEnabled($type) {
    return in_array($type, $this->getEnabledTypes());
  }

  /**
   * Gets redirect URL after login.
   *
   * @return \Drupal\Core\Url|null
   *   The redirect URL or NULL if the redirect is not used.
   */
  public function getRedirectUrl() {
    switch ($this->config->get('redirect')) {
      case static::REDIRECT_LOGIN_PAGE:
        return Url::fromRoute('user.login');

      case static::REDIRECT_REGISTRATION_PAGE:
        return Url::fromRoute('user.register');

      case static::REDIRECT_OTHER_URL:
        return Url::fromUserInput($this->config->get('redirect_other_url'));
    }
    return NULL;
  }

  /**
   * Returns whether or not status messages are disabled.
   *
   * @return bool
   *   TRUE if status messages are disabled, FALSE otherwise.
   */
  public function isStatusMessagesDisabled() {
    return (bool) $this->config->get('disable_status_messages');
  }

  /**
   * Gets the message to display after creating node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node associated with the message.
   *
   * @return string|null
   *   The message or NULL if it is not specified.
   */
  public function getMessage(NodeInterface $node) {
    if ($message = $this->config->get('after_message')) {
      return $this->token->replace($message, ['node' => $node]);
    }
    return NULL;
  }

}
