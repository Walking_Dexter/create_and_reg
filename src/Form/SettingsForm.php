<?php

namespace Drupal\create_and_reg\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Path\AliasManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\create_and_reg\CreateAndReg;
use Drupal\Component\Utility\UrlHelper;

/**
 * Settings form for the Create and Register module.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The path alias manager.
   *
   * @var \Drupal\Core\Path\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * SettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Path\AliasManagerInterface $alias_manager
   *   The path alias manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler, AliasManagerInterface $alias_manager) {
    parent::__construct($config_factory);
    $this->moduleHandler = $module_handler;
    $this->aliasManager = $alias_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('path.alias_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'create_and_reg_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'create_and_reg.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('create_and_reg.settings');

    $form['enabled_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled node types'),
      '#description' => $this->t('Select the node types which you want to use with the module.'),
      '#options' => node_type_get_names(),
      '#default_value' => $config->get('enabled_types'),
    ];

    $form['redirect'] = [
      '#type' => 'radios',
      '#title' => $this->t('Redirect the user after the node is created'),
      '#default_value' => $config->get('redirect'),
      '#options' => [
        CreateAndReg::REDIRECT_NONE => $this->t("Don't redirect the user"),
        CreateAndReg::REDIRECT_LOGIN_PAGE => $this->t('Redirect the user to the login page'),
        CreateAndReg::REDIRECT_REGISTRATION_PAGE => $this->t('Redirect the user to the registration page'),
        CreateAndReg::REDIRECT_OTHER_URL => $this->t('Other url'),
      ],
    ];

    $form['redirect_other_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The URL where to redirect'),
      '#default_value' => $config->get('redirect_other_url'),
      '#states' => [
        'visible' => [
          ':input[name="redirect"]' => ['value' => CreateAndReg::REDIRECT_OTHER_URL],
        ],
        'required' => [
          ':input[name="redirect"]' => ['value' => CreateAndReg::REDIRECT_OTHER_URL],
        ],
      ],
      '#element_validate' => [[$this, 'validateRedirectOtherUrl']],
    ];

    $form['disable_status_messages'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable other status messages after the node is created'),
      '#default_value' => $config->get('disable_status_messages'),
      '#description' => $this->t('It will remove all status messages except the added below.'),
    ];

    $form['after_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#description' => $this->t('This message will be displayed for the user after he/she created the content.'),
      '#default_value' => $config->get('after_message'),
    ];

    // Token module support.
    if ($this->moduleHandler->moduleExists('token')) {
      $form['token_help'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => ['node'],
      ];
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * Validate redirect_other_url element.
   *
   * @param array $element
   *   The element structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateRedirectOtherUrl(array $element, FormStateInterface $form_state) {
    if ($form_state->getValue('redirect') == CreateAndReg::REDIRECT_OTHER_URL) {
      $value = $element['#value'];

      if ($value === '') {
        $form_state->setError($element, $this->t('If you select other url redirect, the url field will be required.'));
      }
      elseif (!UrlHelper::isValid($value)) {
        $form_state->setError($element, $this->t('The added url is not valid.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $enabled_types = $form_state->getValue('enabled_types', []);
    $enabled_types = array_values(array_filter($enabled_types));

    $redirect_url = '';
    if ($form_state->getValue('redirect') == CreateAndReg::REDIRECT_OTHER_URL) {
      // Store the normal path instead of the url alias.
      $redirect_url = $form_state->getValue('redirect_other_url', '');
      $normal_path = $this->aliasManager->getPathByAlias($redirect_url);

      if ($redirect_url != $normal_path) {
        $this->messenger()
          ->addStatus($this->t('%link_path has been stored as %normal_path', [
            '%link_path' => $redirect_url,
            '%normal_path' => $normal_path,
          ]));
        $redirect_url = $normal_path;
      }
    }

    $this->config('create_and_reg.settings')
      ->set('enabled_types', $enabled_types)
      ->set('redirect', $form_state->getValue('redirect'))
      ->set('redirect_other_url', $redirect_url)
      ->set('disable_status_messages', $form_state->getValue('disable_status_messages'))
      ->set('after_message', $form_state->getValue('after_message'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
